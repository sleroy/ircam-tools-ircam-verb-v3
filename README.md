> - Retail Price: 199 $ / 169 €
> - [Premium member](https://www.ircam.fr/innovations/abonnements-du-forum/) Price: 99,50 $ / 84,50 € - [Ask for the voucher](https://shop.ircam.fr/index.php?controller=contact)
> - Distributed by [Flux::](https://shop.flux.audio/en_US/products/ircam-verb)
> - [Technical Support](https://support.flux.audio/portal/sign_in)
> - Get you trial [Try](https://shop.flux.audio/en_US/page/trial-request-information)
> - Software available by downloading the [Flux:: Center ](https://www.flux.audio/download/)
> - iLok.com user account is required (USB Smart Key is not required)

![Flux::](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/logos-flux%2Bircam-noir.png)

![IRCAM Verb v3](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/ircam_verb-v3-full.jpg)
## All Features ##

-	Input/Output Gain (-/+ 12 dB) for adjusting the levels before and after processing
-	True bypass control routing the incoming signal direct to the output for a smooth transition between clean and processed signal
-	Dry/Wet control for blending the original signal with the processed signal
-	A set of 3-band filters applied to each part of the time structure for fine tuning the reverberation characteristics.
-	High density setting for selecting standard/high density reverberation engine
-	Decay Time controls for adjusting the decay time as well as the relative decay time of the High/Mid/Low frequencies explicitly
-	Open Sound Control Support
-	Creative Perceptual Reverb Options parameters

### Channel Routing Matrix ### 

Ircam Verb v3 provides ten input and output channels presenting the option for reverberation processing in multi-channel and surround formats, including Dolby Atmos.
With a built in Input/Output (I/O) routing matrix instant flexibility is provided for configuring the I/O in relation to the physical audio monitoring in the control room.

![Routing matrix](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/routing.png)

### General Reverb Controls and Display ###
Early, Cluster and Reverb On/Solo controls, and Room Size, Reverb Start and Reverb Gain controls.

### Room, Early, Cluster and Reverb Filters ###
A set of 3-band filters applied to each part of the time structure for fine tuning the reverberation characteristics

![Room, Early, Cluster and Reberb Filters](https://forum.ircam.fr/media/uploads/Softwares/Ircam%20Tools/earlyclusterroom.png)

### Reverb Options###
Creative reverb options including; Infinite (“deep-freezing” the reverb). Air Absorption/Roll Freq. simulates the frequency dependent air absorption. Modal Density, the frequency “smoothness” of the Verb engine. Panning, virtual source panning-direction offset. Width, panning width. Diffuseness, the directional information of the reverberation.

### Early (Time Structure)###
Early Time Structure controls, with Early shape controlling the amplitude rise or fall of early reflections, Early Min controlling the time at which the early reflections start to appear, Early Max controlling the time at which the early reflections cease to appear and Early Distribution controlling the way early reflections are scattered in time.

### Cluster (Time Structure) ###
Cluster Time Structure controls, with Cluster Min controlling the time at which the Cluster start to appear, Cluster Max controlling the time at which the Cluster cease to appear and Cluster Distribution controlling the way the Cluster is scattered in time.

### Dual Preset Slots and Parameter Morphing ###
The built in preset manager and the preset morphing slider, provides instant and intuitive control of all parameters and controls. In a second, with a simple one-click operation, everything is copied from one of the two preset slots to the other, even during playback.
The two Preset/Parameter slots, A and B, can be loaded with two full set of parameters at the same time, and except for only A/B comparing two sets of parameters, the morphing slider will allow to mix them, and to record the morph with the host automation.

## Compatibility & plugin formats ##

- **Windows** - 7 SP1, 8.1 and 10, all in 64 bits only
- **Mac** OS X (Intel) - All versions from 10.7.5 (64bit only)
- **Ircam Verb v3** is available in the following configurations: AudioUnit (64bit) / VST 2 (64 bit) / VS3 Native+Masscore (Pyramix 10-12) / AAX Native (64bit)
- **Max sample rate (depending on HOST)**: 384 KHz
- **Ambisonic 1-2-3rd order / dolby atmos stem format **
- **Maximum number of channels**: 16

## Hardware Specification ##

A graphic card fully supporting OpenGL 2.0 is required.

- **Windows:** If your computer has an ATi or NVidia graphics card, please assure the latest graphic drivers from the ATi or NVidia website are installed.
- **Mac OS X:** OpenGL 2.0 required - Mac Pro 1.1 & Mac Pro 2.1 are not supported.

## Software license requirements ##

In order to use the software, an iLok.com user account is required (the iLok USB Smart Key is not required).

VS3 license not included in Flux:: standard license. Additional VS3 license for Pyramix & Ovation Native/MassCore 32/64 bit versions available from [Merging Technologies](https://www.merging.com/sales).

> - [Documentation](https://doc.flux.audio/#/en_US/ircam_verb_doc/0_Ircam_Verb)
>
> - Videos
> - [Tutorial:Introduction To Ircam Verb](https://youtu.be/30zc-eeJ1wc)
> - [Tutorial: Using The Advanced Parameters of Ircam Verb](https://youtu.be/eHYWpICbVM0)
> - [Tutorial: Using Ircam Verb For Atmos® Mixing](https://youtu.be/xa_QZHRJIMk)
> - [Comment créer un effet de largeur stéréophonique réaliste sur des sources monos avec le plugin IRCAM VERB V3 ?](https://www.youtube.com/watch?v=9IZqDKJZCYo) par [Jean Loup Pecquais](http://jeanlouppecquais.com/).
> - [TIPS: Preset Manager & Morphing Slider (EN/US)](https://www.youtube.com/watch?v=YxuSwuX-JWE&feature=emb_logo)
